class Comment < ActiveRecord::Base
  belongs_to :post
  validates_presence_of :post_id, presence: true
  validates_presence_of :body, presence: true
end
